import { createApp } from 'vue/dist/vue.esm-bundler'
import {createRouter, createWebHistory} from 'vue-router'
import TechnasiumMain from './components/TechnasiumMain.vue'
import TechnasiumAssignment from './components/TechnasiumAssignment.vue'
import './style.css'
import App from './App.vue'

const routes = [
  { path: '/technasium', component: TechnasiumMain },
  { path: '/technasium/assignment', component: TechnasiumAssignment },
]

const router = createRouter({
    // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
    history: createWebHistory(),
    routes, // short for `routes: routes`
  })

const app  = createApp(App)

app.use(router)
app.mount('#app')

